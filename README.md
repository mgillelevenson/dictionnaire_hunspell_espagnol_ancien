# Dictionnaire d'ancien espagnol pour correcteur HUNSPELL


Ce dépôt contient les fichiers d'un dictionnaire servant à la vérification de la transcription de textes en espagnol ancien. Attention, ce dictionnaire ne vaut que pour un projet qui suit les normes de transcription que j'ai établies pour mon travail de thèse.

Ce dictionnaire est intégrable aux logiciels de traitement de texte comme LibreOffice, de composition typographique comme TeXMaker, ou d'édition XML comme Oxygen XML editor ou jEdit. 


## Licence


Les fichiers sont issus du dictionnaire de formes, lemmes et PoS créé par Sánchez Marcos dans le cadre de sa thèse de doctorat. Je n'ai fait que récupérer les formes et en créer un dictionnaire HUNSPELL. Ce dictionnaire s'appuie à la fois sur le dictionnaire de castillan moderne et ancien de Freeling (http://nlp.lsi.upc.edu/freeling/index.php/node/1). Les deux dictionnaires sont publiés sous *Lesser General Public License For Linguistic Resources*, et c'est donc sous cette license qu'est publié ce dictionnaire (voir fichier adjoint):
- le dictionnaire d'espagnol moderne (https://github.com/TALP-UPC/FreeLing/tree/master/data/es/dictionary/entries) provient du *Spanish Resource Grammar Project* (http://www.upf.edu/pdi/iula/montserrat.marimon/srg.html) de l'Université Pompeu Fabra.
- le dictionnaire d'espagnol ancien (https://github.com/TALP-UPC/FreeLing/tree/master/data/es/es-old/dictionary/entries) a été crée par Cristina Sánchez Marco dans le cadre de sa thèse de doctorat [Sánchez Marco 2012], et se fonde sur le projet *Spanish Resource Grammar Project*.



## Modifications apportées aux fichiers originaux

Enrichissement progressif du dictionnaire par correction de mon corpus de thèse.

## Bibliographie

+ Sánchez Marco, Cristina. « Tracing the development of Spanish participial constructions: An empirical study of semantic change ». Universitat Pompeu Fabra, 2012. https://www.tdx.cat/bitstream/handle/10803/97044/tcsm.pdf?sequence=1.


## À faire


Fondre le fichier de mise à jour dictionnaire (.tdi) dans ledit dictionnaire (.dic). Le fichier .tdi ne semble être propre qu'à Oxygen. 

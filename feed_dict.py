
def main():
    with open("dict_eso/eso.tdi", "r") as new_entries:
        new_entries_list = new_entries.read().split("\n")

    with open("dict_eso/eso_sp_oldesp.dic", "r") as old_entries:
            old_entries_list = old_entries.read().split("\n")

    updated_list = list(set(old_entries_list + new_entries_list))


    with open("dict_eso/eso_sp_oldesp.dic", "w") as output_dict:
        output_dict.truncate(0)
        output_dict.write("\n".join(updated_list))


if __name__ == '__main__':
    main()
